package Clase3y4;
import java.util.Scanner;
public class Ejercicio5 {

	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		
		int Puesto;
		System.out.print("Ingrese su puesto en la competencia: ");
		Puesto = entrada.nextInt();
		if(Puesto == 1)
		{
			System.out.print("Usted obtiene medalla de oro, felicidades!");
		}
		if(Puesto == 2) 
		{
			System.out.print("Usted obtiene medalla de plata, felicidades!");
		}
		if(Puesto == 3)
		{
			System.out.print("Usted obtiene medalla de bronce, felicidades!");
		}
		if(Puesto > 3)
		{
			System.out.print("Usted no obtiene medallas, siga participando");
		}
		if(Puesto < 1)
		{
			System.out.print("El numero ingresado es erroneo");
		}

	}

}
